from sqlalchemy.orm import Session

from app.models import PointModel, RouteModel, engine
from import_points.validators import PointsAndRoutesFile

file = PointsAndRoutesFile()

points = [PointModel(**row.dict()) for row in file.points]


def create():
    s = Session(bind=engine)
    s.add_all(points)
    s.commit()
    s.execute('OPTIMIZE TABLE ya_taxi.points')


if __name__ == '__main__':
    create()
