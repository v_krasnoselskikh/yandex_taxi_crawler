from typing import List, Dict, Any

import yaml
from pydantic import BaseModel


class Point(BaseModel):
    label: str
    latitude: float
    longitude: float

    def get_rll(self):
        return f'{self.latitude},{self.longitude}'



def yaml_config_settings_source() -> Dict[str, Any]:
    """ """
    with open("points.yml", 'r', encoding='utf8') as stream:
        data = yaml.safe_load(stream)
        return data


class PointsAndRoutesFile(BaseModel):
    points: List[Point]

    def __init__(self, **data):
        super().__init__(**yaml_config_settings_source())
