from pydantic import BaseSettings, Field


class ApiConfig(BaseSettings):
    clid: str = Field(..., env='YANDEX_TAXI_CLID')
    apikey: str = Field(..., env='YANDEX_TAXI_API_KEY')

    class Config:
        env_file = '.env'
        env_file_encoding = 'utf-8'
