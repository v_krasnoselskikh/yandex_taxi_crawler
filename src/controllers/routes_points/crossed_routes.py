from sqlalchemy.orm import Session

from app.models import engine

sql_statement = """
INSERT INTO routes(route_id, start_point_id, end_point_id, label, create_date)

select generateUUIDv4(), p1.point_id, p2.point_id, concat('от ', p1.label, ' до ', p2.label), now()
from points p1
         cross join points as p2
where p1.point_id != p2.point_id;
"""


def create_crossed_routes():
    s = Session(bind=engine)
    s.execute(sql_statement)
    s.execute('OPTIMIZE TABLE ya_taxi.routes')


