from typing import List, Dict, Any, Optional
from pydantic import BaseModel, Field, BaseSettings


class ResponseTripInfoTariff(BaseModel):
    class_level: int
    class_name: str
    class_text: str
    min_price: float
    price: float
    price_text: str
    waiting_time: Optional[float]


class ResponseTripInfo(BaseModel):
    currency: str
    distance: Optional[float]
    options: List[ResponseTripInfoTariff]
    time: Optional[float]
    time_text: Optional[str]




