import aiohttp

from app.controllers.api_taxi.validators import ResponseTripInfo
from app.config import ApiConfig

api_config = ApiConfig()


async def get_taxi_info(aio_session: aiohttp.ClientSession, point_from_rll: str, point_to_rll: str) -> ResponseTripInfo:
    """Получит информацию о стоимости

    :param point_from_rll - точка отправления {долгота пункта отправления},{широта пункта отправления}
    :param point_to_rll - точка назначения
    """
    clid = api_config.clid
    apikey = api_config.apikey

    url = 'https://taxi-routeinfo.taxi.yandex.net/taxi_info'
    headers = {'YaTaxi-Api-Key': apikey}
    async with aio_session.get(url,
                               params={
                                   'clid': clid,
                                   'rll': f'{point_from_rll}~{point_to_rll}',
                                   'class': 'econom,business,comfortplus,minivan,vip,express,courier'
                               },
                               headers=headers) as r:
        status_code = r.status

        if status_code != 200:
            raise ConnectionAbortedError(r.text)

        resp_json = await r.json()

        return ResponseTripInfo.parse_obj(resp_json)
