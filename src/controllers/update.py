import asyncio
import datetime
import timeit
from datetime import timedelta
from typing import Tuple

import aiohttp
from clickhouse_sqlalchemy import make_session
from sqlalchemy import select

from app.controllers.api_taxi.validators import ResponseTripInfo
from app.models import engine, PointModel, RouteModel, TripInfoModel
from app.controllers.api_taxi.methods import get_taxi_info


class ApiDataUpdateController:
    def __init__(self):
        self.session = make_session(engine)

    async def get_prices_by_route_id(self, aio_session, route_id: str) -> Tuple[ResponseTripInfo, str]:
        route: RouteModel = self.session.query(RouteModel).get(route_id)

        start_point: PointModel = self.session.query(PointModel).get(route.start_point_id)
        end_point: PointModel = self.session.query(PointModel).get(route.end_point_id)

        result = await get_taxi_info(
            aio_session=aio_session,
            point_from_rll=f"{start_point.longitude},{start_point.latitude}",
            point_to_rll=f"{end_point.longitude},{end_point.latitude}"
        )

        return result, route_id

    def save_price_for_route(self, route_id: str, price_result: ResponseTripInfo):
        for option in price_result.options:
            row = TripInfoModel(
                route_id=route_id,
                date=datetime.datetime.now(),
                class_name=option.class_name,
                class_level=option.class_level,
                class_text=option.class_text,
                min_price=option.min_price,
                price=option.price,
                waiting_time=option.waiting_time,
                time=price_result.time,
                distance=price_result.distance
            )
            self.session.add(row)

    async def get_all_prices(self):
        select_all_route_id = self.session.execute(select(RouteModel.route_id))

        async with aiohttp.ClientSession() as aio_session:
            tasks = []
            for route in select_all_route_id:
                future_task = self.get_prices_by_route_id(aio_session, route.route_id)
                tasks.append(future_task)

            result = await asyncio.gather(*tasks)
            return result

    def save_prices_to_db(self):
        start = timeit.default_timer()
        loop = asyncio.get_event_loop()

        get_all_prices_result = loop.run_until_complete(self.get_all_prices())

        for prices_result, route_id in get_all_prices_result:
            self.save_price_for_route(route_id, prices_result)
        self.session.commit()

        end = timeit.default_timer()
        print(f'load_routes_from_db: {timedelta(seconds=end - start)}')
        return get_all_prices_result
