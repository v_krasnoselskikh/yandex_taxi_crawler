import datetime
import uuid
from sqlalchemy import Column, Integer, String, DateTime, Float, MetaData
from clickhouse_sqlalchemy import get_declarative_base, types, engines

from app.models.connection import engine

metadata = MetaData(schema='ya_taxi')
Base = get_declarative_base(metadata=metadata)


class UserModel(Base):
    __tablename__ = 'users'
    __table_args__ = (engines.ReplacingMergeTree(order_by=['user_id']),)

    user_id = Column(types.UUID, primary_key=True, default=lambda x: str(uuid.uuid4()))
    label = Column(String)


class PointModel(Base):
    """ Точки """
    __tablename__ = 'points'
    __table_args__ = (engines.ReplacingMergeTree(order_by=['point_id']),)

    point_id = Column(types.UUID, primary_key=True)
    user_id = Column(types.UUID)
    label = Column(String, comment='Название точки')
    latitude = Column(Float(8))
    longitude = Column(Float(8))
    create_date = Column(DateTime, comment='Дата добавления', default=datetime.datetime.now)

    def get_rll(self):
        return f'{self.latitude},{self.longitude}'


class RouteModel(Base):
    """ Путь между точек """
    __tablename__ = 'routes'
    __table_args__ = (engines.MergeTree(primary_key='route_id' ,order_by=['route_id', 'start_point_id', 'end_point_id']),)

    route_id = Column(types.UUID, primary_key=True, default=lambda x: str(uuid.uuid4()))
    start_point_id = Column(types.UUID)
    end_point_id = Column(types.UUID)
    label = Column(String, comment='Название дороги')
    create_date = Column(DateTime, comment='Дата добавления', default=datetime.datetime.now)


class UserRoutes(Base):
    """ Связывает Пользователей и пути"""
    __tablename__ = 'users_to_routes'
    __table_args__ = (engines.ReplacingMergeTree(order_by=['user_id', 'route_id']),)

    user_id = Column(types.UUID, primary_key=True)
    route_id = Column(types.UUID, primary_key=True)

    label = Column(String)


class TripInfoModel(Base):
    """ Стоимость поездки """
    __tablename__ = 'routes_trip_info'
    __table_args__ = (
        engines.MergeTree(order_by=['date']),
    )

    route_id = Column(types.UUID, primary_key=True)
    date = Column(DateTime, primary_key=True)
    class_name = Column(String, primary_key=True, comment='Название тарифа на английском.')
    class_level = Column(Integer, comment='Id тарифа')
    class_text = Column(String, comment='Название тарифа на русском')
    min_price = Column(Float(8), comment='Стоимость подачи машины без повышающего коэффициента')
    price = Column(Float(8), comment='Стоимость поездки')
    waiting_time = Column(Integer, comment='Время подачи машины в секундах.')
    time = Column(Integer, comment='Время поездки в секундах.')
    distance = Column(Float, comment='Дистанция поздки')



