from .models import PointModel, RouteModel, UserModel, metadata
from .connection import engine

with engine.connect() as conn:
    conn.execute("CREATE DATABASE IF NOT EXISTS ya_taxi")

metadata.create_all(engine)
