from typing import Optional, List
from fastapi import FastAPI

app = FastAPI()


@app.get('/user/me')
def get_users():
    return {'user_id': 1, 'label': 'Vladimir'}


@app.post('/user/add')
def add_user():
    pass


@app.get('/points')
def get_points() -> List:
    pass


@app.post('/point/add')
def add_points(point):
    pass


@app.delete('/point/{point_id}')
def delete_point(point_id):
    pass


@app.get('/routes/')
def get_routes() -> List:
    pass


@app.post('/route/add')
def add_route():
    pass


@app.delete('/route/{route_id}')
def delete_route():
    pass
